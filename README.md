# IoBuster Test Development Kit for Python

## Downloading and Installing the Python TDK

### Introduction

In this section, you will learn how to download and install the Python TDK
using `poetry` with a Git repository. This method installs the package directly
from the source, ensuring you have the latest version.

### Steps to Download and Install

#### Step 1: Install Using Poetry

To install the IoBuster TDK from the Git repository, use `poetry` with the
following command:

```bash
poetry add git+https://gitlab.com/qa-maestro-public/test-development-kit/python.git@main
```

Replace `@main` with a specific branch or tag if you want to install a
particular version.

##### Available versions

- v0.2 (git+https://gitlab.com/qa-maestro-public/test-development-kit/python.git@v0.2)
- v0.3 (git+https://gitlab.com/qa-maestro-public/test-development-kit/python.git@v0.3)

## Running IoBuster in RPC Mode

### Introduction

To use the Python TDK, you need to run the coordinator in RPC mode. This example
uses TCP mode for communication.

```bash
coordinator# port rpc start all --transport tcp
```

## Using the Python TDK

### Introduction

This section provides a simple example of how to use the Python TDK. The example
demonstrates how to initialize the TDK, probe an NVMe controller, create queue
pairs, and perform read/write operations.

### Example Code

```python
import threading
from iobustertdk import IobusterTdk, Transport, TcpSocketTransportOptions, UnixSocketTransportOptions

def start_workload(tdk, workload_options, thread_id):
    try:
        result = tdk.nvme.start_workload_generator(workload_options)
        print(f"Workload generator result from thread {thread_id}: {result}")
    except Exception as e:
        print(f"An error occurred in thread {thread_id}: {e}")

def example(tdk):
    try:
        # Probe the NVMe controller
        response = tdk.nvme.probe_controller()
        print(f"Probed NVMe controller: {response}")

        response = tdk.utility.system_info()
        print(f"Utility system info: {response}")

        queue_id = tdk.nvme.create_queue_pair()
        print(f"Created NVMe queue pair with ID: {queue_id}")

        res = tdk.nvme.write(0, 1, {
            "nsid": 1,
            "qid": queue_id
        })
        print(f"Write result: {res}")

        res = tdk.nvme.read(0, 1, {
            "nsid": 1,
            "qid": queue_id
        })
        print(f"Read result: {res}")

        # Start workload generator for 60 seconds
        workload_options = {
            'type': 'basic',
            'nsid': 1,
            'queueDepth': 32,
            'xferSize': 4096,
            'duration': 60,
            'workload': 'random',
            'readPercent': 50
        }

        # Start workload generator in 3 threads
        threads = []
        for i in range(3):
            thread = threading.Thread(target=start_workload, args=(tdk, workload_options, i))
            thread.start()
            threads.append(thread)

        # Wait for all threads to complete
        for thread in threads:
            thread.join()

    except Exception as e:
        print(f"An error occurred: {e}")

def main_tcp():
    tdk = IobusterTdk(Transport.TCP_SOCKET, TcpSocketTransportOptions(host='127.0.0.1', port=8110))
    tdk.connect()
    example(tdk)
    tdk.disconnect()

def main_unix():
    tdk = IobusterTdk(Transport.UNIX_SOCKET, UnixSocketTransportOptions(path='/tmp/iob_port0.sock'))
    tdk.connect()
    example(tdk)
    tdk.disconnect()

if __name__ == "__main__":
    main_tcp()
    # main_unix()
```
