from iobustertdk.json_rpc_client import JsonRpcBaseClient
from typing import TypedDict, Optional
from enum import Enum
import base64


class PowerMeasurement(TypedDict):
    current: int
    voltage: int
    power: int


class ManufacturingInfo(TypedDict):
    manufacturer: str
    manufacturedDate: str


class PmuInfo(TypedDict):
    fab: ManufacturingInfo
    assembly: ManufacturingInfo
    serialNumber: str
    version: str


class AdapterInfo(TypedDict):
    fab: ManufacturingInfo
    assembly: ManufacturingInfo
    serialNumber: str
    version: str
    type: str


class DebugBoardInfo(TypedDict):
    fab: ManufacturingInfo
    assembly: ManufacturingInfo
    serialNumber: str
    version: str
    type: str


class SmbusBlockWriteInfo(TypedDict):
    address: int
    command: int
    payload: bytes
    pec: int


class IobusterPmuMethods(Enum):
    INIT = "iobusterPmu.init"
    UNINIT = "iobusterPmu.uninit"
    GET_VERSION = "iobusterPmu.getVersion"
    MEAUSRE_POWER = "iobusterPmu.measurePower"
    GET_PMU_INFO = "iobusterPmu.getPmuInfo"
    GET_ADAPTER_INFO = "iobusterPmu.getAdapterInfo"
    GET_DEBUG_BOARD_INFO = "iobusterPmu.getDebugBoardInfo"
    SET_FAN_SPEED = "iobusterPmu.setFanSpeed"
    GET_FAN_SPEED = "iobusterPmu.getFanSpeed"
    CHANGE_POWER = "iobusterPmu.changePower"
    SEND_SMBUS_BLOCK_WRITE = "iobusterPmu.sendSmbusBlockWrite"
    GET_RECEIVED_SMBUS_BLOCK_WRITE = "iobusterPmu.getReceivedSmbusBlockWrite"
    RESET = "iobusterPmu.reset"
    UPDATE_FIRMWARE = "iobusterPmu.updateFirmware"


class IobusterPmu:
    def __init__(self, client: JsonRpcBaseClient, api_version: int):
        self._api_version = api_version
        self._client = client

    def init(self):
        method = IobusterPmuMethods.INIT.value
        self._client.send_request(method)

    def uninit(self):
        method = IobusterPmuMethods.UNINIT.value
        self._client.send_request(method)

    def get_version(self) -> str:
        method = IobusterPmuMethods.GET_VERSION.value
        return self._client.send_request(method)

    def measure_power(self) -> PowerMeasurement:
        method = IobusterPmuMethods.MEAUSRE_POWER.value
        return self._client.send_request(method)

    def get_pmu_info(self) -> PmuInfo:
        method = IobusterPmuMethods.GET_PMU_INFO.value
        return self._client.send_request(method)

    def get_adapter_info(self) -> AdapterInfo:
        method = IobusterPmuMethods.GET_ADAPTER_INFO.value
        return self._client.send_request(method)

    def get_debug_board_info(self) -> DebugBoardInfo:
        method = IobusterPmuMethods.GET_DEBUG_BOARD_INFO.value
        return self._client.send_request(method)

    def set_fan_speed(self, speed: int):
        method = IobusterPmuMethods.SET_FAN_SPEED.value
        params = [speed]
        self._client.send_request(method, params)

    def get_fan_speed(self) -> int:
        method = IobusterPmuMethods.GET_FAN_SPEED.value
        return self._client.send_request(method)

    def change_power(self, power: str, state: str):
        method = IobusterPmuMethods.CHANGE_POWER.value
        params = [power, state]
        self._client.send_request(method, params)

    def send_smbus_block_write(self, address: int, command: int, payload: bytes):
        method = IobusterPmuMethods.SEND_SMBUS_BLOCK_WRITE.value
        params = [address, command, list(payload)]
        self._client.send_request(method, params)

    def get_received_smbus_block_write(self) -> Optional[SmbusBlockWriteInfo]:
        method = IobusterPmuMethods.GET_RECEIVED_SMBUS_BLOCK_WRITE.value
        response = self._client.send_request(method)
        if response is None:
            return None
        return SmbusBlockWriteInfo(
            address=response["address"],
            command=response["command"],
            payload=bytes(response["payload"]),
            pec=response["pec"],
        )

    def reset(self):
        method = IobusterPmuMethods.RESET.value
        self._client.send_request(method)

    def update_firmware(self, payload: bytes):
        if not self._api_version >= 2:
            raise Exception("Current ioBuster Service does not support update_firmware")
        method = IobusterPmuMethods.UPDATE_FIRMWARE.value
        encoded_str = base64.b64encode(payload).decode("utf-8")
        params = [encoded_str]
        self._client.send_request(method, params)
