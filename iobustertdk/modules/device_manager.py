from iobustertdk.json_rpc_client import JsonRpcBaseClient


class DeviceManager:
    def __init__(self, client: JsonRpcBaseClient):
        self.client = client

    def disable_pcie_link(self):
        """
        Disables PCIe link from the downstream port.
        """
        return self.client.send_request("deviceManager.disablePcieLink")

    def enable_pcie_link(self):
        """
        Enables PCIe link from the downstream port.
        """
        return self.client.send_request("deviceManager.enablePcieLink")

    def get_port_info(self):
        """
        Returns current port information.

        Returns:
            PortInfo: Information about the current port.
        """
        return self.client.send_request("deviceManager.getPortInfo")

    def get_supported_peripherals(self):
        """
        Returns peripherals supported by the current tester.

        Returns:
            PeripheralsInfo: Information about supported peripherals.
        """
        return self.client.send_request("deviceManager.getSupportedPeripherals")

    def load_nvme_driver(self, force_unload=False):
        """
        Load NVMe Driver.

        Parameters:
            force_unload (bool, optional): Unloads NVMe driver if already loaded. Default is False.
        """
        return self.client.send_request("deviceManager.loadNvmeDriver", [force_unload])

    def load_uio_driver(self, force_unload=False, options=None):
        """
        Load UIO driver.

        Parameters:
            force_unload (bool, optional): Unloads UIO driver if already loaded. Default is False.
            options (dict, optional): Additional options for loading the UIO driver.
                - retry (bool, optional): Whether to retry loading the driver. Default is False.
                - retry_timeout_ms (int, optional): Timeout for retrying to load the driver in milliseconds. Default is 30000.
        """
        return self.client.send_request(
            "deviceManager.loadUioDriver", [force_unload, options]
        )
