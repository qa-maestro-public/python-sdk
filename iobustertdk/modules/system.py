from iobustertdk.json_rpc_client import JsonRpcBaseClient
from typing import List, Dict, Optional
import inspect


class System:
    def __init__(self, client: JsonRpcBaseClient):
        self.client = client

    def get_api_version(self):
        """
        Gets RPC server version

        Returns:
            int: RPC server version
        """
        try:
            return self.client.send_request("system.getApiVersion")
        except Exception as e:
            if str(e) == "Method not found":
                return 1
            raise e
