from iobustertdk.json_rpc_client import JsonRpcBaseClient
from typing import Optional, Dict, Any, List
from dataclasses import asdict
from .generic_command import GenericCommandResult, NvmeCommand
from .identify import (
    IdentifyControllerResult,
    IdentifyNamespaceResult,
    IdentifyControllerListResult,
    IdentifyNamespaceListResult,
    IdentifyControllerData,
    IdentifyNamespaceData,
)
import inspect
import base64


class Nvme:
    def __init__(self, client: JsonRpcBaseClient):
        self.client = client

    def probe_controller(self, options=None):
        """
        Probes an NVMe controller. Throws an exception if an NVMe device is not initialized successfully.

        Parameters:
            options (dict): Probe options.
                - pci_link_ready_timeout_ms (int, optional): PCI link ready timeout in milliseconds. Default is 30000.
                - force_driver_reload (bool, optional): Always reload the driver when set. Default is true.
                - retry_driver_reload (bool, optional): Retry driver reload. Default is true.
                - driver_reload_timeout_ms (int, optional): Driver reload timeout in milliseconds. Default is 30000.
                - cmd_timeout (int, optional): Command timeout in milliseconds. Default is 300000.
        """
        default_options = {
            "pci_link_ready_timeout_ms": 30000,
            "force_driver_reload": True,
            "retry_driver_reload": True,
            "driver_reload_timeout_ms": 30000,
            "cmd_timeout": 300000,
        }

        if options is not None:
            default_options.update(options)

        return self.client.send_request("nvme.probeController", [default_options])

    def create_queue_pair(self):
        """
        Creates an IO queue pair.

        Returns:
            int: The created Queue ID.
        """
        return self.client.send_request("nvme.createQueuePair")

    def delete_queue_pair(self, qid: int):
        """
        Deletes an IO queue pair.

        Parameters:
            qid (int): Queue ID to delete.
        """
        return self.client.send_request("nvme.deleteQueuePair", [qid])

    def get_device_info(self):
        """
        Retrieves device information.

        Returns:
            dict: Device information.
        """
        return self.client.send_request("nvme.getDeviceInfo")

    def get_namespace_info(self, nsid):
        """
        Retrieves namespace information.

        Parameters:
            nsid (int): Namespace ID.

        Returns:
            dict: Namespace information.
        """
        return self.client.send_request("nvme.getNamespaceInfo", [nsid])

    def dataset_management(self, ranges: List[Dict[str, int]], options: Dict[str, int]):
        """
        Sends NVM Dataset Management command.

        Parameters:
            ranges (list[dict]): An array of range objects where each range has a length and starting LBA.
                - length (int): Length in logical blocks.
                - start_lba (int): Starting LBA of the range.
            options (dict): Optional parameters for the command.
                - nsid (int, optional): Namespace ID. Default is 0.
                - qid (int, optional): Queue ID. Default is 0.
                - attributes (int, optional): Dataset management attributes. Default is 4.

        Returns:
            dict: An object containing the status and any additional data from the command.
        """
        return self.client.send_request("nvme.datasetManagement", [ranges, options])

    def format_nvm(
        self, nsid: int, lba_format: int, options: Optional[Dict[str, int]] = None
    ):
        """
        Issues NVM Format command to a given namespace.

        Parameters:
            nsid (int): Namespace ID.
            lba_format (int): LBA Format (0 - 15).
            options (dict, optional): Optional parameters.
                - ses (int, optional): Secure Erase Settings. Default is 0.
                - pil (int, optional): Protection Information Location. Default is 0.
                - pi (int, optional): Protection Information. Default is 0.
                - mset (int, optional): Meta Settings. Default is 0.

        Returns:
            dict: Completion queue entry after the command has been executed.
        """
        default_options = {"ses": 0, "pil": 0, "pi": 0, "mset": 0}

        if options is not None:
            default_options.update(options)

        return self.client.send_request(
            "nvme.formatNvm", [nsid, lba_format, default_options]
        )

    def read(self, start_lba, num_blocks, options=None):
        """
        Sends NVM Read command.

        Parameters:
            start_lba (int): Start LBA where the data should be read from.
            num_blocks (int): Number of Logical Blocks to read.
            options (dict, optional): Additional configuration options.
                - nsid (int, optional): Namespace ID. Default is 0.
                - qid (int, optional): Queue ID. Default is 0.

        Returns:
            dict: Completion queue entry after the command has been executed.
        """
        return self.client.send_request("nvme.read", [start_lba, num_blocks, options])

    def reset_controller(self):
        """
        Issues a controller level reset.
        """
        return self.client.send_request("nvme.resetController")

    def set_command_tracker_log(self, filename: str):
        """
        Sets the filename where command history will be dumped.
        """
        return self.client.send_request("nvme.setCommandTrackerLog", [filename])

    def shutdown(self):
        """
        Issues a controller shutdown.
        """
        return self.client.send_request("nvme.shutdown")

    def start_workload_generator(self, options=None):
        """
        Generates IO workloads.

        Parameters:
            options (dict, optional): Optional parameters.
                - type (str, optional): Type of workload generator. Default is "basic".
                - queue_depth (int, optional): Queue Depth. Default is 0.
                - duration (int, optional): Duration in seconds. Default is 0.
                - loops (int, optional): Number of loops to repeat. Default is 1.
                - data_pattern (bytes, optional): Customized data buffer for write commands. Must be multiple of 8 bytes. Default is None.
                - ignore_error (list[dict], optional): Ignored command errors during execution of WorkloadGenerator. Default is None.
                - features (dict, optional): Optional features. Default is None.

        Returns:
            dict: Workload result.
        """
        default_options = {
            "type": "basic",
            "queue_depth": 0,
            "duration": 0,
            "loops": 1,
            "data_pattern": None,
            "ignore_error": None,
            "features": {},
        }

        if options is not None:
            default_options.update(options)

        return self.client.send_request(
            "nvme.startWorkloadGenerator", [default_options]
        )

    def write(self, start_lba, num_blocks, options=None):
        """
        Sends NVM Write command.

        Parameters:
            start_lba (int): Start LBA where the data should be written.
            num_blocks (int): Number of Logical Blocks to write.
            options (dict, optional): Additional configuration options.
                - nsid (int, optional): Namespace ID. Default is 0.
                - qid (int, optional): Queue ID. Default is 0.
                - data_pattern (bytes, optional): Data pattern to fill the LBAs. Must be multiples of 8. Default is None.

        Returns:
            dict: Completion queue entry after the command has been executed.
        """
        return self.client.send_request("nvme.write", [start_lba, num_blocks, options])

    def send_generic_command_no_data_transfer(
        self, qid: int, command: NvmeCommand
    ) -> GenericCommandResult:
        command_dict = asdict(command)
        response = self.client.send_request(
            "nvme.sendGenericCommandNoDataTransfer", [qid, command_dict]
        )
        return GenericCommandResult.from_dict(response)

    def send_generic_command_host_to_controller(
        self, qid: int, command: NvmeCommand, input_data: bytes
    ) -> GenericCommandResult:
        command_dict = asdict(command)
        encoded_input_data = base64.b64encode(input_data).decode("utf-8")
        response = self.client.send_request(
            "nvme.sendGenericCommandHostToController",
            [qid, command_dict, encoded_input_data],
        )
        return GenericCommandResult.from_dict(response)

    def send_generic_command_controller_to_host(
        self, qid: int, command: NvmeCommand, output_size: int
    ) -> GenericCommandResult:
        command_dict = asdict(command)
        response = self.client.send_request(
            "nvme.sendGenericCommandControllerToHost", [qid, command_dict, output_size]
        )
        return GenericCommandResult.from_dict(response)

    def send_generic_command_bidirectional(
        self, qid: int, command: NvmeCommand, input_data: bytes, output_size: int
    ) -> GenericCommandResult:
        command_dict = asdict(command)
        encoded_input_data = base64.b64encode(input_data).decode("utf-8")
        response = self.client.send_request(
            "nvme.sendGenericCommandBidirectional",
            [qid, command_dict, encoded_input_data, output_size],
        )
        return GenericCommandResult.from_dict(response)

    def identify(self, cns: int, cntid: int, nsid: int) -> GenericCommandResult:
        qid = 0
        output_size = 4096
        command = NvmeCommand(opc=0x06, nsid=nsid, cdw10=(cntid << 16) + cns)
        response = self.send_generic_command_controller_to_host(
            qid, command, output_size
        )
        return response

    def identify_controller(self) -> IdentifyControllerResult:
        cns = 1
        cntid = 0
        nsid = 0
        result = self.identify(cns, cntid, nsid)  # returns { "cqe", "buffer" }
        ctrlr_data = IdentifyControllerData.from_bytes(result.output_data)
        return IdentifyControllerResult(
            cqe=result.cqe, raw_data=result.output_data, controller_data=ctrlr_data
        )

    def identify_namespace(self, nsid: int) -> IdentifyNamespaceResult:
        cns = 0
        cntid = 0
        result = self.identify(cns, cntid, nsid)
        ns_data = IdentifyNamespaceData.from_bytes(result.output_data)
        return IdentifyNamespaceResult(
            cqe=result.cqe, raw_data=result.output_data, namespace_data=ns_data
        )

    def identify_active_controllers(
        self, nsid: int, cntid: int = 0
    ) -> IdentifyControllerListResult:
        cns = 0x12
        result = self.identify(cns, cntid, nsid)
        return IdentifyControllerListResult.from_generic_command_result(result)

    def identify_allocated_namespaces(self, nsid: int = 0) -> IdentifyNamespaceResult:
        cns = 0x10
        cntid = 0
        result = self.identify(cns, cntid, nsid)
        data = IdentifyNamespaceData.from_bytes(result.output_data)
        return IdentifyNamespaceResult(
            cqe=result.cqe, raw_data=result.output_data, namespace_data=data
        )

    def identify_active_namespaces(self, nsid: int = 0) -> IdentifyNamespaceListResult:
        cns = 0x02
        cntid = 0
        result = self.identify(cns, cntid, nsid)
        return IdentifyNamespaceListResult.from_generic_command_result(result)

    def identify_allocated_namespace(self, nsid: int) -> IdentifyNamespaceListResult:
        cns = 0x11
        cntid = 0
        result = self.identify(cns, cntid, nsid)
        return IdentifyNamespaceListResult.from_generic_command_result(result)

    def disable_command_tracker(self):
        """
        Disables command history tracker.

        Raises:
            NotImplementedError: If the method is called, indicating that this feature is not supported yet.
        """
        raise NotImplementedError(
            f'"{self.__class__.__name__}.{inspect.currentframe().f_code.co_name}" is not supported yet.'
        )

    def disable_data_validator(self, options=None):
        """
        Disables data validation at a given namespace.

        Parameters:
            options (dict, optional): Optional parameters.
                - nsid (int, optional): Namespace ID. Default is 0xFFFFFFFF.

        Raises:
            NotImplementedError: If the method is called, indicating that this feature is not supported yet.
        """
        raise NotImplementedError(
            f'"{self.__class__.__name__}.{inspect.currentframe().f_code.co_name}" is not supported yet.'
        )

    def enable_command_tracker(self):
        """
        Enables command history tracker.

        Raises:
            NotImplementedError: If the method is called, indicating that this feature is not supported yet.
        """
        raise NotImplementedError(
            f'"{self.__class__.__name__}.{inspect.currentframe().f_code.co_name}" is not supported yet.'
        )

    def enable_data_validator(self, options=None):
        """
        Enables data validation at a given namespace.

        Parameters:
            options (dict, optional): Optional parameters.
                - nsid (int, optional): Namespace ID. Default is 0xFFFFFFFF.
        Raises:
            NotImplementedError: If the method is called, indicating that this feature is not supported yet.
        """
        raise NotImplementedError(
            f'"{self.__class__.__name__}.{inspect.currentframe().f_code.co_name}" is not supported yet.'
        )
