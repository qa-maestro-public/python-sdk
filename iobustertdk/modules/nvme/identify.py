from typing import Any, List
from dataclasses import dataclass, field
from .generic_command import CompletionQueueEntry, GenericCommandResult


def to_ascii_string(data: bytes) -> str:
    s = []
    for b in data:
        if 0x20 <= b <= 0x7F:
            s.append(chr(b))
        else:
            # JavaScript code replaces them with empty chars
            s.append("")
    return "".join(s)


@dataclass
class IdentifyControllerVersion:
    MJR: int  # Major
    MNR: int  # Minor
    TER: int  # Tertiary


@dataclass
class IdentifyControllerData:
    VID: int
    SSVID: int
    SN: str
    MN: str
    FR: str
    RAB: int
    IEEE: int
    CMIC: int
    MDTS: int
    CNTLID: int
    VER: IdentifyControllerVersion
    RTD3R: int
    RTD3E: int
    OAES: int
    OACS: int
    ACL: int
    AERL: int
    FRMW: int
    LPA: int
    ELPE: int
    NPSS: int
    AVSCC: int
    APSTA: int
    WCTEMP: int
    CCTEMP: int
    MTFA: int
    HMPRE: int
    HMMIN: int
    TNVMCAP: int
    UNVMCAP: int
    RPMBS: int
    SQES: int
    CQES: int
    NN: int
    ONCS: int
    FUSES: int
    FNA: int
    VWC: int
    AWUN: int
    AWUPF: int
    NVSCC: int
    ACWU: int
    SGLS: int

    @staticmethod
    def from_bytes(buffer: bytes) -> "IdentifyControllerData":
        """
        Converts a 4096-byte Identify Controller payload into an IdentifyControllerData dataclass.
        """
        # For brevity, we replicate the fields in the order from your JS code.
        # Some fields (like power states) are omitted as in the JS example.

        VID = int.from_bytes(buffer[0:2], "little")
        SSVID = int.from_bytes(buffer[2:4], "little")
        SN = to_ascii_string(buffer[4:24])
        MN = to_ascii_string(buffer[24:64])
        FR = to_ascii_string(buffer[64:72])
        RAB = buffer[72]
        IEEE = buffer[73] + (buffer[74] << 8) + (buffer[75] << 16)
        CMIC = buffer[76]
        MDTS = buffer[77]
        CNTLID = int.from_bytes(buffer[78:80], "little")

        ver_val = int.from_bytes(buffer[80:84], "little")
        VER_MJR = (ver_val >> 16) & 0xFFFF
        VER_MNR = (ver_val >> 8) & 0xFF
        VER_TER = ver_val & 0xFF
        version = IdentifyControllerVersion(MJR=VER_MJR, MNR=VER_MNR, TER=VER_TER)

        RTD3R = int.from_bytes(buffer[84:88], "little")
        RTD3E = int.from_bytes(buffer[88:92], "little")
        OAES = int.from_bytes(buffer[92:96], "little")

        OACS = int.from_bytes(buffer[256:258], "little")
        ACL = buffer[258]
        AERL = buffer[259]
        FRMW = buffer[260]
        LPA = buffer[261]
        ELPE = buffer[262]
        NPSS = buffer[263]
        AVSCC = buffer[264]
        APSTA = buffer[265]
        WCTEMP = int.from_bytes(buffer[266:268], "little")
        CCTEMP = int.from_bytes(buffer[268:270], "little")
        MTFA = int.from_bytes(buffer[270:272], "little")
        HMPRE = int.from_bytes(buffer[272:276], "little")
        HMMIN = int.from_bytes(buffer[276:280], "little")

        t1 = int.from_bytes(buffer[280:284], "little")
        t2 = int.from_bytes(buffer[284:288], "little")
        t3 = int.from_bytes(buffer[288:292], "little")
        t4 = int.from_bytes(buffer[292:296], "little")
        TNVMCAP = t1 + (t2 << 32) + (t3 << 64) + (t4 << 96)

        u1 = int.from_bytes(buffer[296:300], "little")
        u2 = int.from_bytes(buffer[300:304], "little")
        u3 = int.from_bytes(buffer[304:308], "little")
        u4 = int.from_bytes(buffer[308:312], "little")
        UNVMCAP = u1 + (u2 << 32) + (u3 << 64) + (u4 << 96)

        RPMBS = int.from_bytes(buffer[312:316], "little")

        SQES = buffer[512]
        CQES = buffer[513]
        NN = int.from_bytes(buffer[516:520], "little")
        ONCS = int.from_bytes(buffer[520:522], "little")
        FUSES = int.from_bytes(buffer[522:524], "little")
        FNA = buffer[524]
        VWC = buffer[525]
        AWUN = int.from_bytes(buffer[526:528], "little")
        AWUPF = int.from_bytes(buffer[528:530], "little")
        NVSCC = buffer[530]
        ACWU = int.from_bytes(buffer[532:534], "little")
        SGLS = int.from_bytes(buffer[536:540], "little")

        return IdentifyControllerData(
            VID=VID,
            SSVID=SSVID,
            SN=SN,
            MN=MN,
            FR=FR,
            RAB=RAB,
            IEEE=IEEE,
            CMIC=CMIC,
            MDTS=MDTS,
            CNTLID=CNTLID,
            VER=version,
            RTD3R=RTD3R,
            RTD3E=RTD3E,
            OAES=OAES,
            OACS=OACS,
            ACL=ACL,
            AERL=AERL,
            FRMW=FRMW,
            LPA=LPA,
            ELPE=ELPE,
            NPSS=NPSS,
            AVSCC=AVSCC,
            APSTA=APSTA,
            WCTEMP=WCTEMP,
            CCTEMP=CCTEMP,
            MTFA=MTFA,
            HMPRE=HMPRE,
            HMMIN=HMMIN,
            TNVMCAP=TNVMCAP,
            UNVMCAP=UNVMCAP,
            RPMBS=RPMBS,
            SQES=SQES,
            CQES=CQES,
            NN=NN,
            ONCS=ONCS,
            FUSES=FUSES,
            FNA=FNA,
            VWC=VWC,
            AWUN=AWUN,
            AWUPF=AWUPF,
            NVSCC=NVSCC,
            ACWU=ACWU,
            SGLS=SGLS,
        )


@dataclass
class NsfeatBits:
    THINP: int
    NSABP: int
    DAE: int
    UIDREUSE: int
    OPTPERF: int


@dataclass
class DpcBits:
    PIT1S: int
    PIT2S: int
    PIT3S: int
    PIIFB: int
    PIILB: int


@dataclass
class DpsBits:
    PIT: int
    PIP: int


@dataclass
class LbaFormat:
    MS: int
    LBADS: int
    RP: int


@dataclass
class IdentifyNamespaceData:
    NSZE: int
    NCAP: int
    NUSE: int
    NSFEAT: NsfeatBits
    NLBAF: int
    FLBAS: int
    MC: int
    DPC: DpcBits
    DPS: DpsBits
    NMIC: int
    RESCAP: int
    FPI: int
    DLFEAT: int
    NVMCAP: int
    LBAF: List[LbaFormat] = field(default_factory=list)

    @staticmethod
    def from_bytes(buffer: bytes) -> "IdentifyNamespaceData":
        """
        Converts a 4096-byte Identify Namespace payload into an IdentifyNamespaceData dataclass.
        """
        NSZE = int.from_bytes(buffer[0:8], "little")
        NCAP = int.from_bytes(buffer[8:16], "little")
        NUSE = int.from_bytes(buffer[16:24], "little")

        nsfeat_val = buffer[24]
        nsfeat_bits = NsfeatBits(
            THINP=(nsfeat_val & 0x1),
            NSABP=(nsfeat_val >> 1) & 0x1,
            DAE=(nsfeat_val >> 2) & 0x1,
            UIDREUSE=(nsfeat_val >> 3) & 0x1,
            OPTPERF=(nsfeat_val >> 4) & 0x1,
        )

        NLBAF = buffer[25]
        FLBAS = buffer[26]
        MC = buffer[27]

        dpc_val = buffer[28]
        dpc = DpcBits(
            PIT1S=(dpc_val >> 0) & 0x1,
            PIT2S=(dpc_val >> 1) & 0x1,
            PIT3S=(dpc_val >> 2) & 0x1,
            PIIFB=(dpc_val >> 3) & 0x1,
            PIILB=(dpc_val >> 4) & 0x1,
        )

        dps_val = buffer[29]
        dps = DpsBits(PIT=(dps_val & 0x7), PIP=(dps_val >> 3) & 0x1)

        NMIC = buffer[30]
        RESCAP = buffer[31]
        FPI = buffer[32]
        DLFEAT = buffer[32]  # as in JS code, storing same value
        # (some specs define these bits differently)

        # NVMCAP is 16 bytes at offset 48
        nvmcap_lo1 = int.from_bytes(buffer[48:52], "little")
        nvmcap_lo2 = int.from_bytes(buffer[52:56], "little")
        nvmcap_hi1 = int.from_bytes(buffer[56:60], "little")
        nvmcap_hi2 = int.from_bytes(buffer[60:64], "little")
        NVMCAP = (
            nvmcap_lo1 + (nvmcap_lo2 << 32) + (nvmcap_hi1 << 64) + (nvmcap_hi2 << 96)
        )

        # LBA Formats array at offset 128, each entry 4 bytes, up to NLBAF+1
        lbaf_list: List[LbaFormat] = []
        for i in range(NLBAF + 1):
            offset = 128 + i * 4
            val = int.from_bytes(buffer[offset : offset + 4], "little")
            ms = val & 0xFFFF
            lbads = (val >> 16) & 0xFF
            rp = (val >> 24) & 0x3
            lbaf_list.append(LbaFormat(MS=ms, LBADS=lbads, RP=rp))

        return IdentifyNamespaceData(
            NSZE=NSZE,
            NCAP=NCAP,
            NUSE=NUSE,
            NSFEAT=nsfeat_bits,
            NLBAF=NLBAF,
            FLBAS=FLBAS,
            MC=MC,
            DPC=dpc,
            DPS=dps,
            NMIC=NMIC,
            RESCAP=RESCAP,
            FPI=FPI,
            DLFEAT=DLFEAT,
            NVMCAP=NVMCAP,
            LBAF=lbaf_list,
        )


@dataclass
class IdentifyControllerResult:
    cqe: CompletionQueueEntry
    raw_data: bytes
    controller_data: IdentifyControllerData


@dataclass
class IdentifyNamespaceResult:
    cqe: CompletionQueueEntry
    raw_data: bytes
    namespace_data: IdentifyNamespaceData


@dataclass
class IdentifyNamespaceListResult:
    cqe: CompletionQueueEntry
    raw_data: bytes
    namespaces: List[int]

    @staticmethod
    def from_generic_command_result(
        result: GenericCommandResult,
    ) -> "IdentifyNamespaceListResult":
        namespaces = []
        for i in range(0, 4096, 4):
            nsid = int.from_bytes(result.output_data[i : i + 4], "little")
            if nsid == 0:
                break
            namespaces.append(nsid)
        return IdentifyNamespaceListResult(
            cqe=result.cqe, raw_data=result.output_data, namespaces=namespaces
        )


@dataclass
class IdentifyControllerListResult:
    cqe: CompletionQueueEntry
    raw_data: bytes
    controllers: List[int]

    @staticmethod
    def from_generic_command_result(
        result: GenericCommandResult,
    ) -> "IdentifyControllerListResult":
        controllers = []
        for i in range(0, 4096, 2):
            cntid = int.from_bytes(result.output_data[i : i + 2], "little")
            if cntid == 0:
                break
            controllers.append(cntid)
        return IdentifyControllerListResult(
            cqe=result.cqe, raw_data=result.output_data, controllers=controllers
        )
