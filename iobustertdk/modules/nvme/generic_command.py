from typing import Optional, Dict, Any
from dataclasses import dataclass


@dataclass
class NvmeCommand:
    """
    Represents an NVMe Command.
    Any fields not specified by the user default to 0.
    """

    nsid: int
    opc: int
    cdw10: int = 0
    cdw11: int = 0
    cdw12: int = 0
    cdw13: int = 0
    cdw14: int = 0
    cdw15: int = 0


@dataclass
class CompletionStatus:
    sc: int  # Status Code
    sct: int  # Status Code Type
    m: int  # More
    p: int  # Phase Tag
    dnr: int  # Do Not Retry


@dataclass
class CompletionQueueEntry:
    cdw: int
    sqhd: int
    sqid: int
    cid: int
    status: CompletionStatus


@dataclass
class GenericCommandResult:
    """Parsed result of an NVMe command."""

    cqe: CompletionQueueEntry
    output_data: Optional[bytes] = None

    @staticmethod
    def from_dict(result: Dict[str, Any]) -> "GenericCommandResult":
        """
        Parse the JSON RPC response into a GenericCommandResult dataclass.
        """
        # Extract cqe and status
        cqe_dict = result.get("cqe", {})
        status_dict = cqe_dict.get("status", {})

        cqe = CompletionQueueEntry(
            cdw=cqe_dict.get("cdw", 0),
            sqhd=cqe_dict.get("sqhd", 0),
            sqid=cqe_dict.get("sqid", 0),
            cid=cqe_dict.get("cid", 0),
            status=CompletionStatus(
                sc=status_dict.get("sc", 0),
                sct=status_dict.get("sct", 0),
                m=status_dict.get("m", 0),
                p=status_dict.get("p", 0),
                dnr=status_dict.get("dnr", 0),
            ),
        )

        # If outputData is present, convert it to bytes
        output_data_list = result.get("outputData")
        output_data = bytes(output_data_list) if output_data_list is not None else None

        return GenericCommandResult(cqe=cqe, output_data=output_data)
