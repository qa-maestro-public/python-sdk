from iobustertdk.json_rpc_client import JsonRpcBaseClient


class PowerController:
    def __init__(self, client: JsonRpcBaseClient):
        self.client = client

    def disable_power_monitor(self):
        """
        Stops monitoring power.
        """
        return self.client.send_request("powerController.disablePowerMonitor")

    def enable_power_monitor(self, logname):
        """
        Continuously monitors power and writes monitored data to a file.

        Parameters:
            logname (str): Log descriptor name for a logger.
        """
        return self.client.send_request("powerController.enablePowerMonitor", [logname])

    def init(self, manufacturer="auto", options=None):
        """
        Initializes Power Controller.

        Parameters:
            manufacturer (str, optional): Power controller manufacturer. Default is 'auto'.
                Available manufacturers:
                - 'auto'
                - 'generic'
                - 'ardent'
                - 'qamaestro'
            options (dict): Power controller options. The following options are common across all power controllers:
                - on_hold_time_ms (int, optional): Default on hold time value. Default is 2000 ms.
                - off_hold_time_ms (int, optional): Default off hold time value. Default is 2000 ms.
        """
        if options is None:
            options = {}
        return self.client.send_request("powerController.init", [manufacturer, options])

    def power_cycle(self, options=None):
        """
        Turns off and on the attached device.

        Parameters:
            options (dict, optional): Power cycle options.
                - on_hold_time_ms (int, optional): Power on hold time in milliseconds. Default is 2000 ms.
                - off_hold_time_ms (int, optional): Power off hold time in milliseconds. Default is 2000 ms.
        """
        if options is None:
            options = {}
        return self.client.send_request("powerController.powerCycle", [options])

    def power_off(self, options=None):
        """
        Turns off the attached device using the attached power controller.

        Parameters:
            options (dict, optional): Power off options.
                - off_hold_time_ms (int, optional): Power off hold time in milliseconds. Default is 2000 ms.
        """
        if options is None:
            options = {}
        return self.client.send_request("powerController.powerOff", [options])

    def power_on(self, options=None):
        """
        Turns on the attached device using the attached power controller.

        Parameters:
            options (dict, optional): Power on options.
                - on_hold_time_ms (int, optional): Power on hold time in milliseconds. Default is 2000 ms.
        """
        if options is None:
            options = {}
        return self.client.send_request("powerController.powerOn", [options])

    def uninit(self):
        """
        Uninitializes the power controller.
        """
        return self.client.send_request("powerController.uninit")
