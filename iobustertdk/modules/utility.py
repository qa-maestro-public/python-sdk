from iobustertdk.json_rpc_client import JsonRpcBaseClient


class Utility:
    def __init__(self, client: JsonRpcBaseClient):
        self.client = client

    def system_info(self):
        """
        Returns system information.

        Returns:
            dict: System information.
        """
        return self.client.send_request("utility.systemInfo")
