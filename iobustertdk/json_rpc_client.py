import json
import socket
import threading
from queue import Queue
from typing import Dict


class JsonRpcBaseClient:
    def __init__(self, socket: socket.socket):
        self._request_id = 0
        self._lock = threading.Lock()
        self._condition = threading.Condition(self._lock)
        self._response_map: Dict[int, Dict] = dict()
        self._notification_queue = Queue()
        self._running = False
        self._socket = socket
        self._read_buffer = bytes()

    def connect(self):
        self._connect_socket()
        self._start_thread()

    def disconnect(self):
        self._disconnect_socket()
        self._stop_thread()

    def _connect_socket(self):
        raise Exception("This must be implemented by a child class")

    def _disconnect_socket(self):
        self._socket.shutdown(
            socket.SHUT_RDWR
        )  # Shut down the connection to unblock the recv
        self._socket.close()

    def _start_thread(self):
        if self._running:
            raise Exception("Cannot start thread if already running")

        self._running = True
        self._response_thread = threading.Thread(target=self._handle_incoming_data)
        self._response_thread.daemon = True
        self._response_thread.start()

    def _stop_thread(self):
        if not self._running:
            raise Exception("Cannot stop thread if already stopped")

        self._running = False
        self._response_thread.join()

    def send_request(self, method, params=None):
        if not self._running:
            raise Exception("TDK isn't connected")
        with self._lock:
            self._request_id += 1
            request_id = self._request_id

        request = {
            "jsonrpc": "2.0",
            "method": method,
            "params": params or [],
            "id": request_id,
        }
        request_data = json.dumps(request).encode("utf-8") + b"\0"
        self._socket.sendall(request_data)
        return self._wait_for_response(request_id)

    def _wait_for_response(self, id: int):
        response = None
        with self._condition:
            while id not in self._response_map:
                self._condition.wait()
            response = self._response_map.pop(id)
        if "error" in response:
            message = response["error"].get("message", "Unknown Error")
            raise Exception(message)
        return response.get("result")

    def _handle_incoming_data(self):
        while self._running:
            try:
                # NOTE: recv is blocking
                data = self._socket.recv(4096)
                if data is None:
                    # TODO: Handle disconnection
                    break
                elif len(data) == 0:
                    break
                else:
                    self._read_buffer += data
                    while b"\0" in self._read_buffer:
                        response_data, remaining = self._read_buffer.split(b"\0", 1)
                        self._read_buffer = remaining
                        self._parse_incoming_data(response_data.decode("utf-8"))
            except socket.error:
                self._running = False

    def _parse_incoming_data(self, incoming_data_str):
        incoming_data = json.loads(incoming_data_str)
        if "id" in incoming_data:
            id = incoming_data["id"]
            with self._lock:
                self._response_map[id] = incoming_data
                self._condition.notify_all()
        else:
            # NOTE: We don't expect any notifications for now
            self._notification_queue(incoming_data)


class JsonRpcTcpClient(JsonRpcBaseClient):
    def __init__(self, host, port):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._host = host
        self._port = port
        super().__init__(sock)

    def _connect_socket(self):
        self._socket.connect((self._host, self._port))


class JsonRpcUnixSocketClient(JsonRpcBaseClient):
    def __init__(self, path):
        sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        self._path = path
        super().__init__(sock)

    def _connect_socket(self):
        self._socket.connect(self._path)
