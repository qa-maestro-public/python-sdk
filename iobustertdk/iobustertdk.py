from .json_rpc_client import JsonRpcTcpClient, JsonRpcUnixSocketClient
from .modules.device_manager import DeviceManager
from .modules.nvme import Nvme
from .modules.power_controller import PowerController
from .modules.utility import Utility
from .modules.iobuster_pmu import IobusterPmu
from .modules.system import System
from enum import Enum, auto
from typing import TypedDict, Union, cast


class Transport(Enum):
    UNIX_SOCKET = auto()
    TCP_SOCKET = auto()


class UnixSocketTransportOptions(TypedDict):
    path: str


class TcpSocketTransportOptions(TypedDict):
    host: str
    port: int


class IobusterTdk:
    device_manager: DeviceManager
    nvme: Nvme
    power_controller: PowerController
    utility: Utility
    iobuster_pmu: IobusterPmu
    system: System

    def __init__(
        self,
        transport: Transport,
        options: Union[UnixSocketTransportOptions, TcpSocketTransportOptions],
    ):
        if transport == Transport.TCP_SOCKET:
            options = cast(TcpSocketTransportOptions, options)
            self._client = JsonRpcTcpClient(options["host"], options["port"])
        elif transport == Transport.UNIX_SOCKET:
            options = cast(UnixSocketTransportOptions, options)
            self._client = JsonRpcUnixSocketClient(options["path"])
        else:
            raise Exception("Invalid transport")

        self.device_manager = DeviceManager(self._client)
        self.nvme = Nvme(self._client)
        self.power_controller = PowerController(self._client)
        self.utility = Utility(self._client)

    def connect(self) -> "IobusterTdk":
        self._client.connect()
        self.system = System(self._client)
        api_version = self.system.get_api_version()
        self.iobuster_pmu = IobusterPmu(self._client, api_version)
        return self

    def disconnect(self):
        self._client.disconnect()
