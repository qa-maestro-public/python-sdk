def test_init(tdk):
    tdk.power_controller.init()


def test_power_off(tdk):
    tdk.power_controller.power_off()


def test_power_on(tdk):
    tdk.power_controller.power_on()


def test_power_cycle(tdk):
    tdk.power_controller.power_cycle()


def test_uninit(tdk):
    tdk.power_controller.uninit()
