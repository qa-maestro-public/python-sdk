import threading


def test_probe_controller(tdk):
    tdk.nvme.probe_controller()


def test_create_queue_pair(tdk):
    tdk.nvme.create_queue_pair()


def test_write(tdk):
    tdk.nvme.write(0, 1, {"nsid": 1})


def test_read(tdk):
    tdk.nvme.read(0, 1, {"nsid": 1})


def test_workload(tdk):
    workload_options = {
        "type": "basic",
        "nsid": 1,
        "queueDepth": 32,
        "xferSize": 4096,
        "duration": 60,
        "workload": "random",
        "readPercent": 50,
    }
    tdk.nvme.start_workload_generator(workload_options)


def start_workload(tdk, workload_options, thread_id):
    try:
        result = tdk.nvme.start_workload_generator(workload_options)
        print(f"Workload generator result from thread {thread_id}: {result}")
    except Exception as e:
        print(f"An error occurred in thread {thread_id}: {e}")


def test_workload_multi_threaded(tdk):
    workload_options = {
        "type": "basic",
        "nsid": 1,
        "queueDepth": 32,
        "xferSize": 4096,
        "duration": 60,
        "workload": "random",
        "readPercent": 50,
    }

    threads = []
    for i in range(3):
        thread = threading.Thread(
            target=start_workload, args=(tdk, workload_options, i)
        )
        thread.start()
        threads.append(thread)

    # Wait for all threads to complete
    for thread in threads:
        thread.join()
