def test_system_info(tdk):
    response = tdk.system.get_api_version()
    assert isinstance(response, int), "response must be int"
