def test_init(tdk):
    tdk.iobuster_pmu.init()


def test_get_version(tdk):
    version = tdk.iobuster_pmu.get_version()
    assert type(version) == str


def test_measure_power(tdk):
    power = tdk.iobuster_pmu.measure_power()
    assert "current" in power
    assert "voltage" in power
    assert "power" in power


def test_get_pmu_info(tdk):
    info = tdk.iobuster_pmu.get_pmu_info()
    assert "assembly" in info
    assert "fab" in info
    assert "serial" in info
    assert "version" in info


def test_get_adapter_info(tdk):
    try:
        info = tdk.iobuster_pmu.get_adapter_info()
        print(info)
    except Exception as e:
        print(str(e))
        pass


def test_get_debug_board_info(tdk):
    try:
        info = tdk.iobuster_pmu.get_debug_board_info()
        print(info)
    except Exception as e:
        print(str(e))
        pass


def test_set_fan_speed(tdk):
    try:
        tdk.iobuster_pmu.set_fan_speed(100)
    except Exception as e:
        print(str(e))
        pass


def test_get_fan_speed(tdk):
    try:
        info = tdk.iobuster_pmu.get_fan_speed()
        print(info)
    except Exception as e:
        print(str(e))
        pass


def test_change_power(tdk):
    tdk.iobuster_pmu.change_power("12V", "OFF")
    tdk.iobuster_pmu.change_power("12V", "ON")


def test_reset(tdk):
    tdk.iobuster_pmu.reset()


def test_send_smbus_block_write(tdk):
    address = 0x3A
    command = 0x0F
    first_payload = bytes(
        [
            0x21,
            0x01,
            0x00,
            0x00,
            0x8B,
            0x84,
            0x10,
            0x00,
            0x00,
            0x06,
            0x03,
            0x01,
            0x00,
            0x00,
            0x00,
            0x00,
            0x00,
            0x00,
            0x00,
            0x00,
            0x00,
            0x00,
            0x00,
            0x00,
            0x00,
            0x00,
            0x00,
            0x00,
            0x00,
            0x00,
            0x00,
            0x00,
            0x00,
            0x04,
            0x00,
            0x00,
            0x00,
            0x14,
            0x00,
            0x00,
            0x00,
            0x00,
            0x00,
            0x00,
            0x00,
            0x00,
            0x00,
            0x00,
            0x00,
            0x01,
            0x00,
            0x00,
            0x00,
            0x00,
            0x00,
            0x00,
            0x00,
            0x00,
            0x00,
            0x00,
            0x00,
            0x00,
            0x00,
            0x00,
            0x00,
            0x00,
            0x00,
            0x00,
            0x00,
        ]
    )
    second_payload = bytes(
        [0x21, 0x01, 0x00, 0x00, 0x5B, 0x00, 0x00, 0x00, 0x00, 0x4A, 0xC3, 0x2C, 0xFA]
    )
    tdk.iobuster_pmu.send_smbus_block_write(address, command, first_payload)
    tdk.iobuster_pmu.send_smbus_block_write(address, command, second_payload)


def test_get_received_smbus_block_write(tdk):
    response = tdk.iobuster_pmu.get_received_smbus_block_write()
    assert response is not None
    assert response["address"] == 0x20
    assert response["command"] == 0x0F
    assert response["pec"] == 0xF1


def test_uninit(tdk):
    tdk.iobuster_pmu.uninit()
