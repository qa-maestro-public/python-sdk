import pytest
from iobustertdk import IobusterTdk, Transport, TcpSocketTransportOptions


def pytest_addoption(parser):
    parser.addoption(
        "--host",
        action="store",
        default="172.0.0.1",
        help="TDK host",
    )

    parser.addoption(
        "--port",
        type=int,
        action="store",
        default=8110,
        help="TDK port",
    )


@pytest.fixture(scope="session")
def tdk(request) -> IobusterTdk:  # type: ignore
    host = request.config.getoption("--host")
    port = request.config.getoption("--port")
    tdk = IobusterTdk(
        Transport.TCP_SOCKET, TcpSocketTransportOptions(host=host, port=port)
    )
    tdk.connect()
    yield tdk
    tdk.disconnect()
