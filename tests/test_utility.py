def test_system_info(tdk):
    response = tdk.utility.system_info()

    assert isinstance(response, dict), "responseect must be a dictionary"

    assert "cpus" in response, "Key 'cpus' is missing"
    assert isinstance(response["cpus"], list), "Value of 'cpus' must be a list"

    for cpu in response["cpus"]:
        assert isinstance(cpu, dict), "Each item in 'cpus' must be a dictionary"

        assert "model" in cpu, "Key 'model' is missing in one of the CPUs"
        assert isinstance(cpu["model"], str), "Value of 'model' must be a string"

        assert "speed" in cpu, "Key 'speed' is missing in one of the CPUs"
        assert isinstance(cpu["speed"], int), "Value of 'speed' must be an integer"

        assert "times" in cpu, "Key 'times' is missing in one of the CPUs"
        assert isinstance(cpu["times"], dict), "Value of 'times' must be a dictionary"

        times = cpu["times"]
        assert "user" in times, "Key 'user' is missing in 'times'"
        assert isinstance(times["user"], int), "Value of 'user' must be an integer"

        assert "nice" in times, "Key 'nice' is missing in 'times'"
        assert isinstance(times["nice"], int), "Value of 'nice' must be an integer"

        assert "sys" in times, "Key 'sys' is missing in 'times'"
        assert isinstance(times["sys"], int), "Value of 'sys' must be an integer"

        assert "idle" in times, "Key 'idle' is missing in 'times'"
        assert isinstance(times["idle"], int), "Value of 'idle' must be an integer"

        assert "irq" in times, "Key 'irq' is missing in 'times'"
        assert isinstance(times["irq"], int), "Value of 'irq' must be an integer"

    assert "hostname" in response, "Key 'hostname' is missing"
    assert isinstance(response["hostname"], str), "Value of 'hostname' must be a string"

    assert "platform" in response, "Key 'platform' is missing"
    assert isinstance(response["platform"], str), "Value of 'platform' must be a string"

    assert "type" in response, "Key 'type' is missing"
    assert isinstance(response["type"], str), "Value of 'type' must be a string"

    assert "totalmem" in response, "Key 'totalmem' is missing"
    assert isinstance(
        response["totalmem"], int
    ), "Value of 'totalmem' must be an integer"

    assert "uptime" in response, "Key 'uptime' is missing"
    assert isinstance(response["uptime"], int), "Value of 'uptime' must be an integer"

    assert "port" in response, "Key 'port' is missing"
    assert isinstance(response["port"], int), "Value of 'port' must be an integer"

    assert "version" in response, "Key 'version' is missing"
    assert isinstance(response["version"], str), "Value of 'version' must be a string"

    assert "buildVersion" in response, "Key 'buildVersion' is missing"
    assert isinstance(
        response["buildVersion"], str
    ), "Value of 'buildVersion' must be a string"
